import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-comthree',
  templateUrl: './comthree.component.html',
  styleUrls: ['./comthree.component.css']
})
export class ComthreeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToComponentTwo() {

    this.router.navigateByUrl('/comtwo');
  }

}
