import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComthreeComponent } from './comthree.component';

describe('ComthreeComponent', () => {
  let component: ComthreeComponent;
  let fixture: ComponentFixture<ComthreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComthreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComthreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
