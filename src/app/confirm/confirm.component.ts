import { Component, OnInit } from '@angular/core';
import {Person} from '../registration/person';
import {ManagePersonService} from '../manage-person.service';
import * as moment from 'jalali-moment';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {

  constructor(private personManager: ManagePersonService) { }

  personArray: Person[];
  person: Person;

  ngOnInit() {
    this.personArray = this.personManager.fetchPerson();
    if (this.personArray) {
      this.person = this.personArray[0];
    }

  }

}
