export class Person {
  firstName: string;
  lastName: string;
  age: number;
  mobileNumber: string;
  description: string;
  birthDate: any;
  education: any;
}

